from django.contrib import admin
from django.contrib.auth import get_user_model
# from baquara.users.admin import MyUserAdmin
# from aea.models import aeaProfile


# User = get_user_model()
# # Define an inline admin descriptor for Employee model
# # which acts a bit like a singleton
# class aeaProfileInline(admin.StackedInline):
#     model = aeaProfile
#     can_delete = False
#     verbose_name_plural = 'aea'

# # Define a new User admin
# class SindesepUserAdmin(MyUserAdmin):
#     inlines = (aeaProfileInline, )

# # Re-register UserAdmin
# admin.site.unregister(User)
# admin.site.register(User, SindesepUserAdmin)