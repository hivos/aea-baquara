from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response

from discussion.serializers import BaseTopicSerializer, BaseCommentSerializer, TopicLikeSerializer, CommentLikeSerializer
from baquara.users.serializers import GroupSerializer, GroupAdminSerializer
from courses.models import Class, Course

from django.contrib.auth import get_user_model

User = get_user_model()
