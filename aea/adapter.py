from django.conf import settings
from django.shortcuts import resolve_url

from allauth.account.adapter import DefaultAccountAdapter


class aeaAdapter(DefaultAccountAdapter):

    def get_login_redirect_url(self, request):
        user = request.user

        must_change_password = user.check_password(user.cpf) \
                                or user.check_password('mudar@123') \
                                or user.check_password('mudar123') \
                                or user.check_password('senhasenha') \
                                or user.check_password('senha')

        if must_change_password:
            return resolve_url('/accounts/password/change/')
        else:
            return super().get_login_redirect_url(request)
