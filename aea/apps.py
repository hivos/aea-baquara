from django.apps import AppConfig


class aeaConfig(AppConfig):
    name = 'aea'
    verbose_name = 'aea'
