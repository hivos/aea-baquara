from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import transaction

from courses.models import Course, CourseProfessor
from courses.classes.models import Class
from discussion.models import Forum, Category

import csv

User = get_user_model()


class Command(BaseCommand):
    args = 'file'
    help = 'import users'

    def add_arguments(self, parser):
        parser.add_argument('path', type=str)

    def get_or_create_user(self, name, email, cpf, phone=None):
        # Find out if the user being currently imported already is in the database
        # Email is considered a suitable key for this task
        if User.objects.filter(email=email).exists():
            user = User.objects.get(email=email)
            if user.username != user.cpf:
                user.username = user.cpf
        elif User.objects.filter(cpf=cpf).exists():
            user = User.objects.get(cpf=cpf)
            if user.username != user.cpf:
                user.username = user.cpf
            user.email = email
        else:
            # If the user can't be found, it must be created
            # username = row['email'][:28].split('@')[0]
            user = User.objects.create(
                username=cpf,  # django has a 30 char limitation in the following fields
                cpf=cpf,
                name=name,
                first_name=name.split(' ', 1)[0],
                last_name=name.split(' ', 1)[1],
                email=email,
            )
            if phone:
                user.phone_number=phone
            user.accepted_terms = False
            # created_users+=1
            user.set_password(user.cpf)

        return user

    @transaction.atomic
    def handle(self, *files, **options):

        # if not len(files) == 1:
        #     raise CommandError('Choose a file to import')
        path = options['path']

        # course_makota = Course.objects.get(slug='espaco-formativo-makota-valdina')
        # course_freire = Course.objects.get(slug='espaco-formativo-paulo-freire')

        # Create or get every possible group in wich users can be put in
        group_geral, _ = Group.objects.get_or_create(name='Geral')
        group_aea, _ = Group.objects.get_or_create(name='Equipe aea')
        group_formadores, _ = Group.objects.get_or_create(name='Formadores')
        # group_formadores_makota, _ = Group.objects.get_or_create(name='Formadores Makota')
        group_formadores_freire, _ = Group.objects.get_or_create(name='Formadores Paulo Freire')
        group_apoio_pedagogico, _ = Group.objects.get_or_create(name='Apoio Pedagógico')

        forum_categories = [
            Category.objects.get(name='Geral'),
            Category.objects.get(name='Dúvidas sobre a plataforma'),
            Category.objects.get(name='Percurso formativo'),
            Category.objects.get(name='Outros'),
        ]

        created_users = 0
        updated_users = 0
        # This file specifies wich users must be activated and in wich groups they must be put
        # Only one file is expected, but this operation can be repeated for several files
        with open(path, 'r') as csvfile:
            readf = csv.DictReader(csvfile)
            for row in readf:

                user = self.get_or_create_user(
                    row['nome'].strip(),
                    row['email'].strip(),
                    row['cpf'].replace('.', '').replace('-','').strip(),
                    row['celular'].replace('-','').replace('(','').replace(')','').strip(),
                )

                # TODO Apagar território, pois é o mesmo que NTE.
                # group_territorio, _ = Group.objects.get_or_create(name=row['territorio'].strip())
                group_nte, _ = Group.objects.get_or_create(name=row['nte'].strip())

                user.groups.add(
                    group_geral,
                    # group_territorio,
                    group_nte,
                )

                gorups_cities = []
                for city in row['municipio'].split(','):
                    group_city, _ = Group.objects.get_or_create(name=city.strip())
                    gorups_cities.append(group_city)
                user.groups.add(*gorups_cities)

                if row['cargo_funcao'].strip() == 'Formador  de Equipe Técnica':
                    group_formadores_nte, _ = Group.objects.get_or_create(name='Formadores '+row['nte'].strip())
                    
                    groups_formadores = [
                        group_formadores,
                        group_formadores_freire,
                        group_formadores_nte,
                        # group_turma,
                    ]
                    user.groups.add(*groups_formadores)

                    # course_professor, _ = CourseProfessor.objects.get_or_create(
                    #     user=user,
                    #     course=course_freire,
                    # )
                    # course_professor.role = 'asssitant'
                    # course_professor.save()

                    if Group.objects.filter(name=row['turma'].strip()).exists():
                        group_turma = Group.objects.get(name=row['turma'].strip())
                        group_turma.name = 'Turma - ' + row['turma'].strip()
                        group_turma.save()
                    else:
                        group_turma, _ = Group.objects.get_or_create(name='Turma - '+row['turma'].strip())

                    forum, _ = Forum.objects.get_or_create(title='Forum - Turma '+row['turma'].strip())
                    forum.groups.add(
                        group_turma,
                        group_aea,
                    )

                    user.groups.add(group_turma)
                    forum.category.set(forum_categories)
                    forum.save()

                    # klass, _ = Class.objects.get_or_create(
                    #     name=row['turma'].strip(),
                    #     course=course_freire,
                    # )
                    # klass.assistants.add(user)
                    # klass.save()

                user.is_active = True
                user.save()

                if row.get('nome_apoio', None):
                    user_apoio = self.get_or_create_user(
                        row['nome_apoio'].strip(),
                        row['email_apoio'].strip(),
                        row['cpf_apoio'].replace('.', '').replace('-','').strip(),
                    )

                    user_apoio.groups.add(*groups_formadores)
                    user_apoio.groups.add(*gorups_cities)
                    user_apoio.groups.add(group_apoio_pedagogico)
                    user_apoio.groups.add(group_turma)

                    group_apoio_pedagogico, _ = Group.objects.get_or_create(
                        name='Apoio pedagógico - '+row['nome_apoio'].strip().split(' ', 1)[0],
                    )
                    user_apoio.groups.add(group_apoio_pedagogico)

                    # Adiciona Formadores que são apoiados por este usuário.
                    user.groups.add(group_apoio_pedagogico)

                    # course_professor, _ = CourseProfessor.objects.get_or_create(
                    #     user=user_apoio,
                    #     course=course_freire,
                    # )
                    # klass.assistants.add(user_apoio)
                    # klass.save()

                    user_apoio.save()

            self.stdout.write(self.style.SUCCESS('Usuários Criados: {}\n\n'.format(created_users)))
            self.stdout.write(self.style.SUCCESS('Usuários Atualizados: {}\n\n'.format(updated_users)))
            self.stdout.write(self.style.SUCCESS('Arquivo importado com Sucesso!!!'))
