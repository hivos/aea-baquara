from django.conf.urls import url, include
from django.views.generic import TemplateView
from rest_framework import routers

from . import views


router = routers.SimpleRouter(trailing_slash=False)

app_name = 'aea'

urlpatterns = [
    # include new baquara URLs before baquaras
    url(r'^', include('courses_frontend.urls')),
    
    # include baquara URLs
    url(r'^', include('config.urls')),
    
    # customized apis
    url(r'^api/', include(router.urls)),

]
