import json

from braces.views import LoginRequiredMixin
from django.conf import settings
from django.http import HttpResponse, Http404
from django.utils.decorators import method_decorator
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, DetailView
from django.core.exceptions import PermissionDenied
from django.contrib.auth import get_user_model

from rest_framework import viewsets, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny
from rest_framework.viewsets import ModelViewSet

from courses.models import Course, CourseStudent, Class, StudentProgress
from courses.permissions import IsProfessorCoordinatorOrAdminPermissionOrReadOnly

from discussion.models import Comment, CommentLike, Topic, TopicLike
from rest_pandas import PandasViewSet
from rest_pandas.renderers import PandasCSVRenderer, PandasJSONRenderer
import pandas as pd

from courses.workspaces.serializers import CourseGroupSerializer
from courses_learning_objects.models import LearningObject, Answer
from django.db.models import Count


User = get_user_model()
