(function(angular){
    'use strict';

    angular.module('login', [
        'login.controllers',
        'login.services',
        'core.services',
        'djangular',
        'ui.bootstrap',
        'ui.tinymce',
        'shared',
    ]);
})(angular);
