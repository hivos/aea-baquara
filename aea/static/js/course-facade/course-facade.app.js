(function(angular) {
    'use strict';

    angular.module('course-facade', [
        'shared',
        'course-facade.controller',
        'eventsList',
        'ngFileUpload',
        'ui.tinymce',
    ]);
})(angular);
