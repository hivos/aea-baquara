
(function (angular) {
    'use strict';
    /* Services */
    angular.module('notes.services', ['ngResource']).
        factory('Note', function($resource){
            return $resource(BASE_API_URL + '/note/:note_id', {}, {
                update: {method: 'PUT'},
                get: {method: 'GET', isArray: true}
            });
        }).
        factory('CourseUserNotes', function($resource){
            return $resource(BASE_API_URL + '/user_notes/:course_slug', {}, {
            });
        }).
        factory('UserNotes', function($resource){
            return $resource(BASE_API_URL + '/user_notes/', {}, {
            });
        });
})(angular);
