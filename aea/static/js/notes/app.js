
(function (angular) {
    'use strict';

    // Declare app level module which depends on filters, and services
    angular.module('notes', [
        'notes.controllers',
        'notes.services',
        'core.services',
        'django',
        'gettext',
        'ui.bootstrap',
        'shared',
    ]);
})(angular);
