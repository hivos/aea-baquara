(function(angular){
    'use strict';
    var app = angular.module('chat.controllers', []);

    app.controller('ChatCtrl', [
        'UserAccess',
        function (UserAccess) {

            // Log this access
            (new UserAccess({ area: 'chat-main-page' })).$save();

        }
    ]);

})(window.angular);
