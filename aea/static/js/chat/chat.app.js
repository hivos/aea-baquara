(function(angular){
    'use strict';

    var app = angular.module('chat', [
        'django',
        'core.services',
        'djangular',
        'shared',
        'ui.bootstrap',
        'chat.controllers',
    ]);

})(angular);
